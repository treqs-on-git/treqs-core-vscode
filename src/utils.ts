import * as vscode from 'vscode';
import util = require('util');
const exec = util.promisify(require('child_process').exec);
import { tableView } from './webviews';

/**
 * executes a terminal command and returns the stdout
 * @param command a treqs terminal command
 * @returns the stdout split into an array
 */
export const execCommand = async (command: string): Promise<string[]> => {
	let res = await exec(command).catch((err: Error) => {
		return err;
	});
	return res.stdout.split('\n');
};

/**
 * filters the stdout of a treqs list terminal command
 * @param resArray the stdout of treqs list
 * @returns a filtered array that contains only relevant lines
 */
export const filterList = (resArray: string[]): string[] => {
	return resArray.filter((line: string) => {
		if (line.includes('|')) {
			return line;
		}
	});
};

/**
 * CheckError is a data type used to describe the stderr of
 * treqs check
 */
export type CheckError = {
	errorType: string;
	identifier: any;
	path?: RegExpMatchArray | null;
	parent?: string[] | string | undefined;
	reference?: string[] | string | undefined;
	linkType?: string[] | undefined;
	pointsTo?: string[] | undefined;
	shouldPointTo?: string[] | undefined;
};

/**
 * takes in an array of CheckErrors, splits the contents into arrays
 * and transforms them into markdown tables
 * @param resArray is an array of CheckErrors
 * @returns an object containing two arrays, each array containing a markdown table
 */
export const makeMarkdown = (resArray: CheckError[]) => {
	let elemArray: string[] = ['| Element | Error | Location |\n|---|---|---|'];
	let linkArray: string[] = ['| Type | Within | Error | Location |\n|---|---|---|---|'];
	let refArray: string[] = ['| Element | Error | Referenced within |\n|---|---|---|'];
	let pointsArray: string[] = ['| Type | From | To | Error | Points to | Should point to |\n|---|---|---|---|---|---|'];
	resArray.forEach((error) => {
		if (error.linkType) {
			pointsArray.push(`|${error.linkType}|${error.identifier}|${error.parent}|${error.errorType}|${error.pointsTo}|${error.shouldPointTo}|`);
		} else if (error.parent) {
			linkArray.push(`|${error.identifier}|${error.parent}|${error.errorType}|${error.path}|`);
		} else if (error.reference) {
			refArray.push(`|${error.identifier}|${error.errorType}|${error.reference}|`);
		} else {
			elemArray.push(`|${error.identifier}|${error.errorType}|${error.path}|`);
		};
	});
	return {elems: elemArray, links: linkArray, refs: refArray, points: pointsArray};
};

/**
 * takes in the stderr of treqs check, filters out the errors and casts them into the
 * CheckError data type.
 * @param resArray the stderr of treqs check
 * @returns an array of CheckErrors
 */
export const filterCheck = (resArray: string[]): CheckError[] => {
	let mapped: CheckError[] = [];
	resArray.forEach((line: string) => {
		let uid = line.match(/[a-f0-9-]{32,36}/g);
		let path = line.match(/\\.*md|\/.*md/g);
		if (line.includes('is duplicated in')) {
			mapped.push({errorType: 'Duplicate UID at', identifier: uid, path});
		} else if (line.includes('does not have the right type in')) {
			mapped.push({errorType: 'Invalid element type at', identifier: uid, path});
		} else if (line.includes('does not have an id in')) {
			let elemType = line.match(/\|\s[a-z]*\s\|/g);
			let stripped = elemType?.map((type) => {
				return type.replace(/[\|\s]/g, '');
			});
			mapped.push({errorType: 'Missing UID at', identifier: stripped, path});
		} else if (line.includes('Unrecognized type:')) {
			let elemType = line.match(/:\s[a-z-]*\s\(/g);
			let stripped = elemType?.map((type) => {
				if (type === ': (') {
					return " ";
				} else {
					return type.replace(/[:\s\(]/g, '');
				}
			});
			mapped.push({errorType: 'Unrecognized element type at', identifier: stripped, path});
		} else if (line.includes('Unrecognized link type')) {
			let linkType = line.match(/type\s.*\swithin/g);
			let parent = line.match(/of type\s.*\s\(/g);
			let stripped = linkType?.map((type) => {
				return type.replace(/\b(?:type | within)\b/g, '');
			});
			let strippedParent = parent?.map((parent) => {
				return parent.replace(/\b(?:of type\s)\b|\s\(/g, '');
			});
			mapped.push({errorType: 'Unrecognized link type at', identifier: stripped, path, parent: strippedParent});
		} else if (line.includes('Required links missing')) {
			mapped.push({errorType: 'Links missing at', identifier: uid, path});
		} else if (line.includes('does not exist')) {
			if (uid !== undefined && uid !== null) {
				if (uid.length === 1) {
					let referenced = line.match(/\|\s[a-z0-9-_]*\s\|/g);
					let stripped = referenced?.map((id) => {
						return id.replace('| ', '').replace(' |', '');
					});
					mapped.push({errorType: 'Element does not exist', identifier: stripped, reference: uid[0]});
				} else {
					mapped.push({errorType: 'Element does not exist', identifier: uid[0], reference: uid[1]});
				}
			}
		} else if (line.includes('needs to point to')) {
			let linkType = line.match(/'.*'/g);
			let badTarget = line.match(/type\s.*,/g);
			let corrTarget = line.match(/a\s.*\sinstead/g);
			let strippedLink = linkType?.map((word) => {
				return word.replace(/'/g, '');
			});
			let strippedBadTarget = badTarget?.map((word) => {
				return word.replace(/\b(?:type\s)\b|,/g, '');
			});
			let strippedCorrTarget = corrTarget?.map((word) => {
				return word.replace(/\b(?:a | instead)\b/g, '');
			});
			if (uid !== undefined && uid !== null) {
				mapped.push({errorType: 'Invalid target type', identifier: uid[0], parent: uid[1], linkType: strippedLink, pointsTo: strippedBadTarget, shouldPointTo: strippedCorrTarget});
			}
		}
	});
	return mapped;
};

/**
 * takes in an array of CheckErrors and transforms the errors into an array containing
 * a dot digraph (dotArray). dotArray is then passed into createBuffer that transforms
 * it into a Uint8Array.
 * @param errArray is an array of CheckErrors
 * @returns a Uint8Array containing a dot digraph
 */
export const makeDot = (errArray: CheckError[]): Uint8Array => {
    let dotArray: string[] = [`digraph {\nrankdir = "LR";\nnode [style="solid,filled", shape=box, fontname="Arial"];
DuplicateID [color="#d77355", label="Duplicate UID at"];
NoID [color="#ff613f", label="Missing UID at"];
wrongType [color="#508cd7", label="Invalid element type at"];
UnrecognizedType [color="#e6c86e", label="Unrecognized element type at"];
UnrecognizedLinkType [color="#64b964 ", label="Unrecognized link type at"];
LinkMissing [color="#32a897", label="Link missing at"]
NonExistent [color="#c66eff", label="Points to non-existent element"]
InvalidTarget [color="#ff4b3f", label="Should point to"]\n`];
    errArray.forEach((err) => {
		if (err.path !== undefined) {
			dotArray.push(`"${err.path}" [color= lightgrey, label="${err.path}"]\n`);
		} else {
			(err.pointsTo !== undefined) 
			? dotArray.push(`"${err.pointsTo}" [color = lightgrey, label="${err.pointsTo}"]\n`)
			: dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"]\n`);
		}
        if (err.errorType === 'Duplicate UID at') {
            dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"];\n"${err.identifier}" -> DuplicateID [color = "#d77355", arrowhead = none];\n`);
			dotArray.push(`DuplicateID -> "${err.path}" [color= "#d77355", arrowhead = none]\n`);
        } else if (err.errorType === 'Invalid element type at') {
            dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"];\n"${err.identifier}" -> wrongType [color="#508cd7", arrowhead = none];\n`);
			dotArray.push(`wrongType -> "${err.path}" [color= "#508cd7", arrowhead = none]\n`);
        } else if (err.errorType === 'Missing UID at') {
            dotArray.push(`${err.identifier} [color = lightgrey, label="${err.identifier}"];\n${err.identifier} -> NoID [color="#ff613f", arrowhead = none];\n`);
			dotArray.push(`NoID -> "${err.path}" [color= "#ff613f", arrowhead = none];\n`);
        } else if (err.errorType === 'Unrecognized element type at') {
            dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"];\n"${err.identifier}" -> UnrecognizedType [color="#e6c86e", arrowhead = none];\n`);
			dotArray.push(`UnrecognizedType -> "${err.path}" [color= "#e6c86e", arrowhead = none];\n`);
        } else if (err.errorType === 'Unrecognized link type at') {
            dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"];\n"${err.identifier}" -> UnrecognizedLinkType [color="#64b964", arrowhead = none];\n`);
			dotArray.push(`UnrecognizedLinkType -> "${err.path}" [color= "#64b964", arrowhead = none];\n`);
        } else if (err.errorType === 'Links missing at') {
            dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"];\n"${err.identifier}" -> LinkMissing [color="#32a897", arrowhead = none];\n`);
			dotArray.push(`LinkMissing -> "${err.path}" [color= "#32a897", arrowhead = none]\n`);
        } else if (err.errorType === 'Element does not exist') {
			dotArray.push(`"${err.reference}" [color = lightgrey, label="${err.reference}"];\n"${err.reference}" -> NonExistent [color="#c66eff", arrowhead = none];\n`);
			dotArray.push(`NonExistent -> "${err.identifier}" [color= "#c66eff", arrowhead = none]\n`);
		} else if (err.errorType === 'Invalid target type') {
			dotArray.push(`"${err.identifier}" [color = lightgrey, label="${err.identifier}"];\n"${err.identifier}" -> InvalidTarget [color="#ff4b3f", arrowhead = none];\n`);
			dotArray.push(`InvalidTarget -> "${err.pointsTo}" [color= "#ff4b3f", arrowhead = none]\n`);
		}
    });
    dotArray.push('}');
    let uIntArr = createBuffer(dotArray.join(''));
    return uIntArr;
};

/**
 * Converts a string into a Uint8Array
 * @param parsedString A digraph in string format
 * @returns A digraph as a Uint8Array
 */
export const createBuffer = (parsedString: string): Uint8Array => {
	let buf = new ArrayBuffer(parsedString.length);
	let uIntBuf = new Uint8Array(buf);
	for (let i = 0; i < parsedString.length; i++) {
		uIntBuf[i] = parsedString.charCodeAt(i);
	}
	return uIntBuf;
};

/**
 * creates and displays a webview panel
 * @param mdAsHtml filtered stdout as html table
 */
 export const createView = (mdAsHtml: string) => {
	const panel = vscode.window.createWebviewPanel(
		'tableView',
		'TReqs',
		vscode.ViewColumn.Two,
		{}
		);

		/** insert html into webview */
		panel.webview.html = tableView(mdAsHtml);
};