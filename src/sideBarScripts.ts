export const sideBarScript: string =
`
const tsvscode = acquireVsCodeApi();
function rotateArrow(name){
  if (document.getElementById(name).style.transform === "rotate(0deg)"){
    document.getElementById(name).style.transform = "rotate(270deg)";
    if (name === 'create_arrow'){
      document.getElementById("create").innerHTML = document.getElementById("create").innerHTML.split('<style>')[0]
  }
  }
  else{
    if (name === 'create_arrow'){
      document.getElementById("create").innerHTML += '<style> @media screen and (max-width: 260px) { #create { height: 538px !important;}}</style>'
  }
    document.getElementById(name).style.transform = "rotate(0deg)";
  }
  
}
function showDiv(name) {
  if (document.getElementById(name).style.display === "block"){
    document.getElementById(name).style.display = "none";
  }
  else{
    document.getElementById(name).style.display = "block";
  }
}
function createTreqElem(){
  tsvscode.postMessage({
  type: 'createElement',
  value: {
          "label": document.getElementById("element_label").value,
          "type": document.getElementById("element_type").value
          }
  });
  document.getElementById("element_label").value = "";
  document.getElementById("element_type").value = "";
}

function createTreqLink(){
  tsvscode.postMessage({
    type: 'createLink',
    value: {
      "link":document.getElementById("element_link").value,
      "target": document.getElementById("element_target").value
    }
    });
    document.getElementById("element_link").value = "";
    document.getElementById("element_target").value = "";
}

function generateTreq(){
   tsvscode.postMessage({
   type: 'generateId',
   value: {
      "amount": document.getElementById("amount").value
    }
   });
   document.getElementById("amount").value = "";
 }

function listTreq(){
  tsvscode.postMessage({
  type: 'list',
  value: {
          "pattern": document.getElementById("pattern").value,
          "type": document.getElementById("type").value,
          "uid": document.getElementById("uid").value
        }
  });
  document.getElementById("pattern").value = "";
  document.getElementById("type").value = "";
  document.getElementById("uid").value = "";
}
`;
