import * as vscode from 'vscode';
import util = require('util');
const exec = util.promisify(require('child_process').exec);
import { MarkdownString, Uri } from 'vscode';

/**
 * The GoHoverProvider class encapsulates content shown in hover previews.
 * @function provideHover gets the position of the word that the cursor is hovering above and checks if
 * the line the word is in contains a treqs-link. Treqs list with the --uid flag is then used to find the
 * target treqs-element. Differing messages are shown if the link has 1 target, multiple targets or no target.
 */
export class GoHoverProvider implements vscode.HoverProvider {
    public async provideHover( document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken) {
        /** Create new instance of MarkdownString (required by Hover) */
		const mdStr = new MarkdownString();
		mdStr.appendCodeblock(`<treqs-link/>`, 'xml');
		mdStr.appendMarkdown('___\n');
        /** Get the range of the word that the cursor is on */
		let range = document.getWordRangeAtPosition(position);
		if (range !== undefined) {
            /** Get the line that the cursor is on */
			let line = document.lineAt(position.line).text;
			if (line.includes('<treqs-link')) {
                let folders = vscode.workspace.workspaceFolders;
                if (folders !== undefined) {
                    let uid = line.match(/[a-f0-9-]{32,36}/g);
                    /** if uid === null the treqs-link has no target uid */
                    if (uid !== null) {
                        /** await execList (executes treqs list --uid and processes stdout) */
						let resArray = await execList(folders[0].uri.path, uid[0]);
                        /** if resArray.length !== 1 the target uid is duplicated */
                        if (resArray.length !== 1) {
                            mdStr.appendMarkdown(`Target uid: ${uid} is duplicated\n`);
                            resArray.forEach((line) => {
                                mdStr.appendMarkdown(`\n[${line[line.length - 1]}](${Uri.file(line[line.length - 1].replace('at ', ''))})\n`);
                            });
                            mdStr.appendMarkdown(`\n___\nClick [here](command:TReqs.check) to call TReqs: Check Table`);
                        } else {
                            resArray.forEach((line) => {
                                mdStr.appendCodeblock(`<treqs-element id=${line[0]} type=${line[1]}>\n\t${line[2]}\n</treqs-element>`, 'xml');
                                mdStr.appendMarkdown(`[${line[3]}](${Uri.file(line[3].replace('at ', ''))})\n`);
                            });
                        }
					} else {
                        console.log('No uid found');
                        mdStr.appendMarkdown(`This link has no target.`);
                        mdStr.appendMarkdown(`\n___\nClick [here](command:TReqs.list) to call TReqs: List`);
                    }
                    mdStr.isTrusted = true;
                    /** return an instance of Hover containing the MarkdownString */
					return new vscode.Hover(mdStr);
				} else {
                    console.log('No workspace');
                }
			}
		} else {
            console.log('range = undefined');
        }
    }
}

/**
 * execList executes treqs list --uid and processes the stdout
 * @param path the root of the workspace
 * @param uid the uid of the target element
 * @returns a nested array where each subarray contains the uid, type, label and
 * location of a treqs-element.
 */
export const execList = async (path: string, uid: string): Promise<string[][]> => {
	const { stdout, stderr } = await exec(`treqs list ${path} --uid ${uid} 2>&1`);
	let resArray = stdout.split('\n');
	resArray = resArray.filter((line: string) => {
		if (line.includes('|')) {
			return line;
		}
	}).map((line: string) => {
		return line.split('|').map((word) => {
			return word.trim();
		}).filter((word) => {
			if (word !== '') {
				return word;
			}
		});
	});
	return resArray;
};
