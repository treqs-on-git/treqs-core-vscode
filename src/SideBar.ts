import { getNonce } from "./getNonce";
import { sideBarStyle } from "./sideBarStyles";
import { sideBarScript } from "./sideBarScripts";
const nonce = getNonce();

export const sideBar: string =
  `<!DOCTYPE html>
  <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Security-Policy" content=""; img-src https: data:; style-src 'unsafe-inline'; script-src 'nonce-${nonce}';">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>${sideBarStyle}</style>
      </head>
      <body>
      <div>
      <input type="checkbox" id="btnControl1" class="hiddenCheck" onclick="showDiv('create_container')"/>
        <label class="btn" id="lable1" for="btnControl1">
            <div class="item" id="create">
              <div onclick="rotateArrow('create_arrow')">
                  <h2 class="title">Create</h2>
                  <div>
                    <svg class="arrow" id="create_arrow" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M7.976 10.072l4.357-4.357.62.618L8.284 11h-.618L3 6.333l.619-.618 4.357 4.357z"/>
                    </svg>
                  </div>
              </div>
              <div id="create_container" style="display:none;" class="answer_list">
                  <h3> Create T-reqs element </h3>
                  <h4> Which type should the element have? </h4>
                  <input id="element_type" placeholder="Requirement"> </input>
                  <h4> Which label should the element have </h4>
                  <input id="element_label" placeholder="Label"> </input>
                  <button class="create_button" onclick="createTreqElem()"> Create </button>
                  <h3> Create T-reqs link </h3>
                  <h4>Which type should the link have?</h4>
                  <input id="element_link" placeholder="relatesTo"> </input>
                  <h4>What UID does the target treqs element have?</h4>
                  <input id="element_target" placeholder="element id (9a8a...)"> </input>
                  <button class="create_button" onclick="createTreqLink()"> Create </button>
                  <h3> Generate Treqs Id's </h3>
                  <input id="amount" placeholder="Amount"> </input>
                  <button class="create_button" onclick="generateTreq()"> Generate </button>
              </div>
            </div>
        </label>
        <input type="checkbox" id="btnControl2" class="hiddenCheck" onclick="showDiv('list_container')"/>
        <label class="btn" id="lable2" for="btnControl2">
            <div class="item" id="list">
              <h2 class="title" onclick="rotateArrow('list_arrow')">List</h2>
              <svg class="arrow" id="list_arrow" onclick="rotateArrow('list_arrow')" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M7.976 10.072l4.357-4.357.62.618L8.284 11h-.618L3 6.333l.619-.618 4.357 4.357z"/>
              </svg>
              <div id="list_container" style="display:none;" class="answer_list">
                  <h3>Treqs list file pattern</h3>
                  <input class="list_input" id="pattern" placeholder="file pattern (.md, .py., etc)"> </input>
                  <h3>Treqs list element type </h3>
                  <input class="list_input" id="type" placeholder="element type (requirement, test, etc)"> </input>
                  <h3>Treqs list specific element </h3>
                  <input class="list_input" id="uid" placeholder="element id (9a8a....)"> </input>
                  <button class="list_button" onclick="listTreq()"> List </button>
              </div>
            </div>
            <input type="checkbox" id="btnControl3" class="hiddenCheck" onclick="showDiv('process_container')"/>
        <label class="btn" id="lable3" for="btnControl3">
            <div class="item" id="process">
              <h2 class="title" onclick="rotateArrow('process_arrow')">Process</h2>
              <svg class="arrow" id="process_arrow" onclick="rotateArrow('process_arrow')" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M7.976 10.072l4.357-4.357.62.618L8.284 11h-.618L3 6.333l.619-.618 4.357 4.357z"/>
              </svg>
              <div id="process_container" style="display:none;" class="answer_list">
                  <h3>Image format</h3>
        <label for="standard-select"></label>
        <div class="select">
        <select id="standard-select">
        <option value="svg">Svg</option>
        <option value="png">Png</option>
        <option value="html">Html</option>
        </select>
        <span class="focus"></span>
        </div>
        <h3>PlantUml service</h3>
        <label for="standard-select"></label>
        <div class="select">
        <select id="standard-select">
        <option value="treqsService<">Treqs service</option>
        <option value="jar">local Jar file</option>
        <option value="plantUml">PlantUml Website</option>
        </select>
        <span class="focus"></span>
        </div>
        <button> 
        Process
        </button>
        </div>
        </div>
        <input type="checkbox" id="btnControl4" class="hiddenCheck" onclick="showDiv('check_container')"/>
        <label class="btn" id="lable4" for="btnControl4">
        <div class="item" id="check">
            <h2 class="title" onclick="rotateArrow('help_arrow')">Check</h2>
            <svg class="arrow" id="check_arrow" onclick="rotateArrow('help_arrow')" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M7.976 10.072l4.357-4.357.62.618L8.284 11h-.618L3 6.333l.619-.618 4.357 4.357z"/>
            </svg>
            <div id="check_container" style="display:none;" class="answer_list">
              <h4>Stuff about check</h4>
              <button> 
              Check
              </button>
            </div>
        </div>
        <input type="checkbox" id="btnControl5" class="hiddenCheck" onclick="showDiv('help_container')"/>
        <label class="btn" id="lable5" for="btnControl5">
        <div class="item" id="help">
            <h2 class="title" onclick="rotateArrow('help_arrow')">Help</h2>
            <svg class="arrow" id="help_arrow" onclick="rotateArrow('help_arrow')" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M7.976 10.072l4.357-4.357.62.618L8.284 11h-.618L3 6.333l.619-.618 4.357 4.357z"/>
            </svg>
            <div id="help_container" style="display:none;" class="answer_list">
              <h3>
                  Documentaions on how to use the <a href="https://gitlab.com/treqs-on-git/treqs-core-vscode/-/blob/main/README.md">extension</a>
              </h3>
              <h3>
                  Documentaions on <a href="https://gitlab.com/treqs-on-git/treqs-ng/-/blob/master/README.md">Treqs </a>
              </h3>
            </div>
        </div>
      </body>
      <script>${sideBarScript}</script>
  </html>`;
