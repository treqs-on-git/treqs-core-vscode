import * as vscode from "vscode";
import * as cp from 'child_process';
import util = require('util');
const exec = util.promisify(require('child_process').exec);
import { getNonce } from "./getNonce";
import { sideBar } from "./SideBar";
import * as markdownObject from 'markdown-it';
import { tableView } from './webviews';
import console = require("console");


export default class SidebarProvider implements vscode.WebviewViewProvider {
  _view?: vscode.WebviewView;
  _doc?: vscode.TextDocument;

  constructor(private readonly _extensionUri: any) { }

  public resolveWebviewView(webviewView: vscode.WebviewView) {
    this._view = webviewView;

    webviewView.webview.options = {
      enableScripts: true, 
      localResourceRoots: [this._extensionUri],
    };
  
    webviewView.webview.html = this._getHtmlForWebview();


    webviewView.webview.onDidReceiveMessage(async (data) => {
      /**
       * executes a terminal corrosponding terminal command to the feature in the sidebar
       * @param data the arguments of a treqs command
       * @returns vscode.window which is either viewed in a previewer or a vsode information message
       */
        switch (data.type) {
          case "createElement": {
            return this.showMessage(await this.callCreateElem(data));
          }
          case "createLink": {
            return this.showMessage(await this.callCreateLink(data));
          }
          case "generateId": {
            return this.showMessage(await this.callGenerateId(data));
          }
          case "list": {
            return this.callList(data);
          }
        }
      });
  }
    public showMessage(response: {message: string, error: boolean}){
        return (response.error ? vscode.window.showErrorMessage(response.message):
        vscode.window.showInformationMessage(response.message));
    }
  
    public async callList(data: any){
      /**
       * assembles the treqs command which will be executed
       * @param data the arguments of a treqs command
       */
    if (!data.value){
      return {message: "Treqs list failed, are you sure that you have Treq's installed", error: true};
    }
    if(vscode.workspace.workspaceFolders !== undefined) {
      let workingDir = vscode.workspace.workspaceFolders[0].uri.path;
      let command = 'treqs list ';

      if(data.value.type !== ''){ // type specified
        command += '--type ' + data.value.type + ' ';
      }
      if(data.value.uid !== ''){ // uid specified
        command += '--uid ' + data.value.uid + ' ';
      }
      if (data.value.pattern !== ''){ // pattern specified
        command += workingDir + '/' + data.value.pattern;
        const { stdout, stderr } = await exec(command + " 2>&1");
          return this.callFilterList(stdout);
      }
      else{ 
        command += workingDir + '/';
        const { stdout, stderr } = await exec(command + " 2>&1");
          return this.callFilterList(stdout);
      }
      } 
      else {
          return {message:'No open workspace found, unable to use treqs list', error: true};
      }
  }

  public callFilterList(response:string){
    if (response !== null){
      let resArray = response.split('\n');
      let md = new markdownObject();
      resArray = resArray.filter((line) => {
        if (line.includes('|')) {
          return line;
        }
      });
      resArray.unshift('| UID | Type | Issue |\n| --- | --- | --- |');
      let mdAsHtml = md.render(resArray.join('\n'));
      const panel = vscode.window.createWebviewPanel(
        'tableView',
        'TReqs: List',
        vscode.ViewColumn.Two,
        {});
      panel.webview.html = tableView(mdAsHtml);
      return {message: mdAsHtml, error: false};
    }
    else{
      return {message:'treqs list failed, could not find treqs', error: true};
    }
  }

  public async callCreateElem(data: any){
    /**
     * assembles the create element command to be executed
     */
    if (!data.value) {
      return {message: "Treqs element creation failed, are you sure that you have Treq's installed", error: true};
    }
    const { stdout, stderr } = await exec('treqs create --tet "' + data.value.type + '" --label "' + data.value.label + '"');
    if (stdout !== null){
      return {message: stdout.toString(), error: false};
    }
    else{
      return {message:'treqs element creation failed, could not find Treqs', error: true};
    }

  }

  public async callCreateLink(data: any){
    /**
     * assembles the create link command to be executed
     */
    if (!data.value) {
      return {message:"Treqs link creation failed, are you sure that you have Treq's installed", error: true};
    }
    const { stdout, stderr } = await exec('treqs createlink --linktype "' + data.value.link + '" --target "' + data.value.target + '"');
      if (stdout !== null){
        return {message: stdout.toString(), error: false};
      }
      else{
        return {message:'treqs link creation fails, could not find Treqs', error: true};
      }
  }

  public async callGenerateId(data: any){
    /**
     * assembles the generated uid command to be executed with the data argument as the amout of id's
     */
    if (!isNaN(data.value.amount) && data.value.amount < 30){
    const { stdout, stderr } = await exec('treqs generateid --amount ' + data.value.amount);
        if (stdout !== null){
          return {message: stdout.toString(), error: false};
        }
        else{
          return {message: 'treqs id generation failed, could not find treqs', error: true};
        }
    }
    else{
      return {message:'treqs id generation failed, amount should be a integer and cannot be more than 30', error: true};
    }
  }
  
  public callProcess(data: any){
    //TODO
  }

  public callCheck(data: any){
  //TODO
  }

  public revive(panel: vscode.WebviewView) {
    this._view = panel;
  }
  private _getHtmlForWebview(){
    // returns the sidebar html to be rendered
    return sideBar;
  }
}
