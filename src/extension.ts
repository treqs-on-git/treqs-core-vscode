'use strict';
import * as vscode from 'vscode';
import * as cp from 'child_process';
import * as markdownObject from 'markdown-it';
import { Uri } from 'vscode';
import  SidebarProvider  from './SideBarProvider';
import { GoHoverProvider } from './hoverProvider';
import { execCommand, filterList, filterCheck,  makeMarkdown, createView , makeDot} from './utils';

export function activate(context: vscode.ExtensionContext) {

	/** get the graphviz-preview extension */
	let graphviz = vscode.extensions.getExtension('EFanZh.graphviz-preview');

	/** Hover provider registered. Hover content is an instance of GoHoverProvider */
	context.subscriptions.push(vscode.languages.registerHoverProvider({ pattern: '**' }, new GoHoverProvider()));

	const config = vscode.workspace.getConfiguration('treqs');
	if (config.get('TreqsBar')){	
		const sidebarProvider = new SidebarProvider(context.extensionUri);
		context.subscriptions.push(
			vscode.window.registerWebviewViewProvider(
				"treqs-sidebar",
				sidebarProvider
			)
		);	
	}
	context.subscriptions.push(vscode.commands.registerCommand('TReqs.list', async (folder) => {
		/** create new instance of markdown-it object */
		let md = new markdownObject();

		/** get directory tree of currently open workspace */
		let folders = vscode.workspace.workspaceFolders;
		if (folders === undefined) {
			vscode.window.showErrorMessage('No open workspace found, unable to use treqs list');
			return;
		} else {
			/** folder === undefined : list was not called on specific file/folder in context menu */
			let URI = (folder !== undefined) ? folder.path : folders[0].uri.path;
			/** execute treqs list using execCommand */
			let resArray = filterList(await execCommand(`treqs list ${URI} 2>&1`));
			/** add .md table header */
			resArray.unshift('| UID | Type | Label |\n| --- | --- | --- |');
			/** transform markdown table into html using markdown-it object */
			let mdAsHtml = md.render(resArray.join('\n'));
			/** create a webview panel */
			createView(mdAsHtml);
		}
	}));

	context.subscriptions.push(vscode.commands.registerCommand('TReqs.process', () => {
		const terminal = (vscode.window.activeTerminal === undefined) ? vscode.window.createTerminal('TReqs terminal') : vscode.window.activeTerminal;
		terminal.show();
		terminal.sendText('treqs process');
	}));

	context.subscriptions.push(vscode.commands.registerCommand('TReqs.check', async (folder) => {
		/** create new instance of markdown-it object */
		let md = new markdownObject();

		/** get directory tree of currently open workspace */
		let folders = vscode.workspace.workspaceFolders;
		if (folders === undefined) {
			vscode.window.showErrorMessage('No open workspace found, unable to use treqs check');
			return;
		} else {
			/** folder === undefined : list was not called on specific file/folder in context menu */
			let URI = (folder !== undefined) ? folder.path : folders[0].uri.path;
			/** execute treqs check using execCommand and pass stdout into filterCheck */
			let resArray = filterCheck(await execCommand(`treqs check ${URI} --ttim ${folders[0].uri.path}/ttim.yaml 2>&1`));
			/** Transform CheckError array into markdown arrays */
			let { elems, links, refs, points } = makeMarkdown(resArray);
			/** transform markdown tables into html using markdown-it object */
			let mdAsHtml = md.render(elems.join('\n'))  + md.render(refs.join('\n')) + md.render(links.join('\n')) + md.render(points.join('\n'));
			/** create a webview panel */
			createView(mdAsHtml);
		}
	}));

	if (config.get('GraphvizOptions' || graphviz !== undefined)){
		context.subscriptions.push(vscode.commands.registerCommand('TReqs.checkDiagram', async (folder) => {
			/** get directory tree of currently open workspace */
			let folders = vscode.workspace.workspaceFolders;
			if (folders === undefined) {
				vscode.window.showErrorMessage('No open workspace found, unable to use treqs check');
				return;
			} else {
				/** folder === undefined : list was not called on specific file/folder in context menu */
				let URI = (folder !== undefined) ? folder.path : folders[0].uri.path;
				/** execute treqs check using execCommand and pass stdout into filterCheck */
				let resArray = filterCheck(await execCommand(`treqs check ${URI} --ttim ${folders[0].uri.path}/ttim.yaml 2>&1`));
				/** get root of the currently open workspace */
				let path = folders[0].uri.path + '/out.dot';
				/** pass res array into makeDot and write the returned array into a file */
				vscode.workspace.fs.writeFile(vscode.Uri.file(path), makeDot(resArray)).then(() => {
					/** open the newly written .dot file */
					vscode.commands.executeCommand('vscode.open', (Uri.file(path))).then(() => {
						/** use graphviz-preview extension to open preview to the side */
						graphviz !== undefined ? vscode.commands.executeCommand('graphviz.showPreviewToSide') : console.log('Graphviz Preview extension not active');
						});
					});
				}
			}));
		};
		

	context.subscriptions.push(vscode.commands.registerCommand('TReqs.createElement', () =>{
			
		let editor = vscode.window.activeTextEditor;
		//check if editor is open
		if(!editor){
			return vscode.window.showErrorMessage('No open workspace found, unable to create the element');
		}else{
			const execShell = cp.exec('treqs create --tet requirement --label Description:', (err, response) =>{
				return vscode.commands.executeCommand("TReqs.insertElement", response);
			});
		}
	}));

	context.subscriptions.push(vscode.commands.registerTextEditorCommand('TReqs.insertElement', function (editor, edit, args) {
        return edit.replace(editor.selection, args);
    }));

	context.subscriptions.push(vscode.commands.registerCommand('TReqs.createLink', () => {
		let editor = vscode.window.activeTextEditor;
		if(!editor){
			return vscode.window.showErrorMessage('No open workspace found, unable to create the link');
		}else{
			const execShell = cp.exec('treqs createlink --linktype relatesTo --target TargetID', (err, response) => {
				return vscode.commands.executeCommand("TReqs.insertElement", response);
			});
		}
	}));
}