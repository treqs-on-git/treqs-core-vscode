import * as assert from 'assert';
import * as vscode from 'vscode';
import { Uri } from 'vscode';
import expect = require('chai');
import path = require('path');
const testFolder = '../../../src/test/examples';

/**
 * A timeout utility function
 * @param ms the length of the timeout
 * @returns a promise that resolves after a timeout
 */
 export const delay = (ms: number): Promise<number> => {
	return new Promise(resolve => setTimeout(resolve, ms));
};

suite('extension.ts test suite', () => {
	vscode.window.showInformationMessage('Start all tests.');

	/**
	 * Close all active editors between tests to avoid interaction effects.
	 */
	teardown(async function() {
		await vscode.commands.executeCommand('workbench.action.closeAllEditors');
	  });
	
	// test("Should start extension @integration", async () => {
	// 	const started = await vscode.extensions.getExtension("undefined_publisher.treqs");
	// 	assert.strictEqual(started?.isActive, true);
	// });

	test('Should execute command treqs.createElement', async () => {
		// get the extension
		const treqs = await vscode.extensions.getExtension("undefined_publisher.treqs");
		// get the test document
		const testDocument = await vscode.workspace.openTextDocument(treqs?.extensionPath + '/src/test/examples/testCreateElement.md');
		// open editor with test file
		const testEditor = await vscode.window.showTextDocument(testDocument);
		// execute command in the editor
		await vscode.commands.executeCommand('TReqs.createElement');

		// wait for vscode to execute function
		await delay(1000);

		// check if extension is active
		assert.strictEqual(treqs?.isActive, true);
		// check if execution of the command took 6 lines
		assert.strictEqual(testEditor.document.lineCount, 6);
		// :TODO: need to mock the uuid to compare output strings.
		// check the length of the output string with the expected
		expect.assert.isAtLeast(testEditor.document.getText().length, 144);
	}).timeout(4000);

	test('Extension should activate on markdown', async () => {
		/** get the extension */
		const treqs = vscode.extensions.getExtension("undefined_publisher.treqs");
		/** assert that the extension is not active */
		assert.strictEqual(treqs?.isActive, true);
	});

	test('TReqs command should activate extension', async () => {
		/** get the extension */
		const treqs = vscode.extensions.getExtension("undefined_publisher.treqs");
		/** execute treqs list */
		await vscode.commands.executeCommand('TReqs.list');
		/** the extension should now be active */
		assert.strictEqual(treqs?.isActive, true);
	});

	test('TReqs: List should open webview', async () => {
		/** get the extension */
		const treqs = vscode.extensions.getExtension("undefined_publisher.treqs");
		/** open an example markdown document */
		await vscode.commands.executeCommand('vscode.open', Uri.file(treqs?.extensionPath + '/src/test/testData/extensionData.md'));
		let active = vscode.window.activeTextEditor?.document.fileName;
		/** FLAKY: Depends on window being in foreground.
		 *  assert that the document was opened and is active (focused) */
		assert.notStrictEqual(active, undefined);
		assert.strictEqual(vscode.window.state.focused, true);
		/** execute treqs list */
		await vscode.commands.executeCommand('TReqs.list');
		/** FLAKY: Depends on window being in foreground.
		 * assert that the markdown document no longer has focus (a webview was opened) */
		assert.notStrictEqual(vscode.window.state.focused, false);
	}).timeout(6000);

	test('TReqs: Check Table should open webview', async () => {
		/** get the extension */
		const treqs = vscode.extensions.getExtension("undefined_publisher.treqs");
		/** open an example markdown document */
		await vscode.commands.executeCommand('vscode.open', Uri.file(treqs?.extensionPath + '/src/test/testData/extensionData.md'));
		/** show the document in an editor window */
		let active = vscode.window.activeTextEditor?.document.fileName;
		/** FLAKY: Depends on window being in foreground.
		 * assert that the document was opened and is active (focused) */
		assert.notStrictEqual(active, undefined);
		assert.strictEqual(vscode.window.state.focused, true);
		/** execute treqs check */
		await vscode.commands.executeCommand('TReqs.check');
		/** FLAKY: Depends on window being in foreground.
		 * assert that the markdown document no longer has focus (a webview was opened) */
		assert.notStrictEqual(vscode.window.state.focused, false);
	});

	test('TReqs: Check Diagram should create a dot file', async () => {
		/** get the extension */
		const treqs = vscode.extensions.getExtension("undefined_publisher.treqs");
		let commands = await vscode.commands.getCommands();
		if (commands.includes('TReqs.checkDiagram')) {
			/** execute treqs check */
			await vscode.commands.executeCommand('TReqs.checkDiagram');

			/** get folders in the workspace */
			const folders = vscode.workspace.workspaceFolders;
			/** assert that there are folders in the workspace */
			assert.notStrictEqual(folders, undefined);
			assert.notStrictEqual(folders?.length, 0);
			if (folders !== undefined) {
				/** open the generated dot file in a text editor */
				const path = folders[0].uri.path + '/out.dot';
				const dotFile = await vscode.workspace.openTextDocument(path);
				const editor = await vscode.window.showTextDocument(dotFile);
				/** assert that the dot file is not empty */
				assert.notStrictEqual(editor.document.lineCount, 0);
			}
		}
	});

	test('Should execute command treqs.createLink', async () => {
		const treqs = await vscode.extensions.getExtension("undefined_publisher.treqs");
		const testDocument = await vscode.workspace.openTextDocument(treqs?.extensionPath + '/src/test/examples/testCreateLink.md');
		const testEditor = await vscode.window.showTextDocument(testDocument);
		await vscode.commands.executeCommand('TReqs.createLink');

		await delay(1000);

		assert.strictEqual(treqs?.isActive, true);
		expect.assert.isOk(testEditor.document.getText(), '<treqs-link type="relatesTo" target="TargetID" />\n');
		expect.assert.isAtLeast(testEditor.document.getText().length, 50);
		assert.strictEqual(testEditor.document.lineCount, 2);
	}).timeout(4000);
});
