import * as assert from 'assert';
import * as vscode from 'vscode';
import * as utils from '../../utils';
import * as chai from 'chai';
import * as promised from 'chai-as-promised';
chai.use(promised);

suite('utils.ts test suite', () => {

	/** test data for treqs list utility functions */
	const stdout = ['file_traverser created', 'treqs_element_factory created', 'list_elements created',
	'Calling XML traversal with filename src/test/testData/extensionData.md',
	   '### Processing elements in File src/test/testData/extensionData.md',
	'| a0820e06961411eabb370242ac130002 | requirement | ### 1.0 The extension shall be tested rigorously | at src/test/testData/extensionData.md',
	'| a0820e06961411eabb370242ac130002 | requirement | ### 2.0 The test data shall conatain a faulty treqs element | at src/test/testData/extensionData.md'];

	/** test data for treqs check utility functions */
	const stderr = ['element with id | a0820e06961411eabb370242ac130002 | is duplicated in src/test/testData/extensionData.md', 
	'treqs check exited with failed checks.'];

	/**
	 * Close all active editors between tests to avoid interaction effects.
	 */
	 teardown(async function() {
		await vscode.commands.executeCommand('workbench.action.closeAllEditors');
	  });

	test('execCommand should resolve', async () => {
		/** get folders in the workspace */
		const folders = vscode.workspace.workspaceFolders;
		/** assert that there are folders in the workspace */
		assert.notStrictEqual(folders, undefined);
		assert.notStrictEqual(folders?.length, 0);
		if (folders !== undefined) {
			return chai.expect(utils.execCommand(`treqs list ${folders[0].uri.path} 2>&1`)).to.be.fulfilled;
		}
	});

	test('execCommand should return stdout in an array', async () => {
		/** get folders in the workspace */
		const folders = vscode.workspace.workspaceFolders;
		/** assert that there are folders in the workspace */
		assert.notStrictEqual(folders, undefined);
		assert.notStrictEqual(folders?.length, 0);
		if (folders !== undefined) {
			let stdout = await utils.execCommand(`treqs list ${folders[0].uri.path} 2>&1`);
			/** assert that the right type is returned */
	        assert(Array.isArray(stdout));
		}
	});

	test('filterList should return only lines that contain |', async () => {
		/** pass stdout into filterList */
		let filtered = utils.filterList(stdout);
		/** assert that the right type is returned */
		assert(Array.isArray(filtered));
		/** assert that the array is the correct length */
		assert.strictEqual(filtered.length, 2);
		/** assert that each line in filtered contains | */
		filtered.forEach((line) => {
			assert.strictEqual(line.includes('|'), true);
		});
	});

	test('filterCheck should transform stdout into array of CheckErrors', async () => {
		/** pass stderr into filterCheck */
		let checkErrors = utils.filterCheck(stderr);
		/** assert that the right type is returned */
		assert(Array.isArray(checkErrors));
		/** assert that the array is the correct length */
		assert.strictEqual(checkErrors.length, 1);
		/** assert that each line in filtered is of type CheckError */
		checkErrors.forEach((line) => {
			chai.assert.typeOf<utils.CheckError>(line, 'object');
		});
	});

	test('makeDot should transform array of CheckErrors into Uint8Array', async () => {
		/** pass stderr into filterCheck */
		let checkErrors = utils.filterCheck(stderr);
		/** assert that the right type is returned */
		assert(Array.isArray(checkErrors));
		/** assert that the array is the correct length */
		assert.strictEqual(checkErrors.length, 1);
		/** assert that each line in filtered is of type CheckError */
		checkErrors.forEach((line) => {
			chai.assert.typeOf<utils.CheckError>(line, 'object');
		});
		/** pass CheckError array into makeDot */
		let uInt = utils.makeDot(checkErrors);
		/** assert that returned value is of type Uint8Array */
		chai.assert.typeOf<Uint8Array>(uInt, 'Uint8Array');
	});

	test('createBuffer should transform a string into a Uint8Array', async () => {
		/** pass stderr into filterCheck */
		let checkErrors = utils.filterCheck(stderr);
		/** assert that the right type is returned */
		assert(Array.isArray(checkErrors));
		/** assert that the array is the correct length */
		assert.strictEqual(checkErrors.length, 1);
		/** assert that each line in filtered is of type CheckError */
		checkErrors.forEach((line) => {
			chai.assert.typeOf<utils.CheckError>(line, 'object');
		});
		/** join CheckErrors into string and pass into createBuffer */
		let errString = checkErrors.join('');
		let uInt = utils.createBuffer(errString);
		/** assert that returned value is of type Uint8Array */
		chai.assert.typeOf<Uint8Array>(uInt, 'Uint8Array');
	});
});