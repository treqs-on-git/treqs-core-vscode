import * as assert from 'assert';
import SidebarProvider from '../../SideBarProvider';
// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode';
import * as path from 'path';
import * as testData from '../testData/sidebarData';
// import * as myExtension from '../../extension';

suite('Extension sidebar Test Suite', () => {
	vscode.window.showInformationMessage('Start all tests.');
	const newSideBar = new SidebarProvider("testingUri");
	test("Should successfully call create element", async() => {
		const response = await newSideBar.callCreateElem({
			"value": {
				"type": "unit-test",
				"label": "unit test for feature in sidebar"
			}});
		const responeNoId = response.message.replace((/[a-f0-9]{32}/g), ""); // replacing the uid before assertion
		assert.strictEqual(response.error, testData.treqsElement.error);
		assert.strictEqual(responeNoId, testData.treqsElement.message);
	});

	test("Should successfully call create element without any parameters", async() => {
		const response = await newSideBar.callCreateElem({
			"value": {
				"type": "",
				"label": ""
			}});
		const responeNoId = response.message.replace((/[a-f0-9]{32}/g), ""); // replacing the uid before assertion
		assert.strictEqual(response.error, testData.emptyTreqsElement.error);
		assert.strictEqual(responeNoId, testData.emptyTreqsElement.message);
	});

	test("Should fail to call create element", async() => {
		const response = await newSideBar.callCreateElem({});
		const responeNoId = response.message.replace((/[a-f0-9]{32}/g), ""); // replacing the uid before assertion
		assert.strictEqual(response.error, testData.errorTreqsElement.error);
		assert.strictEqual(responeNoId, testData.errorTreqsElement.message);
	});

	test("Should successfully call create link", async() => {
		const response = await newSideBar.callCreateLink({
			"value":{
				"link": "relatesTo",
				"target": "20ab4a3ceae411eb8e75acde48001122"
			}});
		assert.strictEqual(response.error, testData.treqsLink.error);
		assert.strictEqual(response.message, testData.treqsLink.message);
	});

	test("Should successfully call create link without any parameters", async() => {
		const response = await newSideBar.callCreateLink({
			"value":{
				"link": "",
				"target": ""
			}});
		assert.strictEqual(response.error, testData.emptyTreqsLink.error);
		assert.strictEqual(response.message, testData.emptyTreqsLink.message);
	});
	
	test("Should fail to call create link", async() => {
		const response = await newSideBar.callCreateLink({});
		assert.strictEqual(response.error, testData.errorTreqsLink.error);
		assert.strictEqual(response.message, testData.errorTreqsLink.message);
	});

	test("Should successfully call generateId", async() => {
		const response = await newSideBar.callGenerateId({
			"value": {
				"amount": 5
			}});
		assert.strictEqual(response.error, testData.uids.error);
		assert.strictEqual(response.message.length, testData.uids.message.length);
	});

	test("Should fail to call generateId due to input type", async() => {
		const response = await newSideBar.callGenerateId({
			"value": {
				"amount": "fifty nine"
			}});
		assert.strictEqual(response.error, testData.uidsError.error);
		assert.strictEqual(response.message, testData.uidsError.message);
	});

	test("Should fail to call generateId due to amount", async() => {
		const response = await newSideBar.callGenerateId({
			"value": {
				"amount": 80
			}});
		assert.strictEqual(response.message, testData.uidsError.message);
	});

	test("Should successfully call list", async() => {
		const response = await newSideBar.callList({
			"value":{
				"type": "unittest",
				"uid": "",
				"pattern": "",
			}
		});
		assert.strictEqual(response.message.length, testData.treqsList.message.length);
		assert.strictEqual(response.message, testData.treqsList.message);
		assert.strictEqual(response.error, testData.treqsList.error);
	});

	test("Should fail to call list", async() => {
		const response = await newSideBar.callList({});
		assert.strictEqual(response.error, testData.errorTreqsList.error);
		assert.strictEqual(response.message, testData.errorTreqsList.message);
	});
});