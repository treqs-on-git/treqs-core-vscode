import * as assert from 'assert';
import * as vscode from 'vscode';
import * as hover from '../../hoverProvider';
import * as chai from 'chai';
import * as promised from 'chai-as-promised';
chai.use(promised);

suite('hoverProvider.ts test suite', () => {

    test('execList should resolve', async () => {
        /** assert that a workspace is open */
        let folders = vscode.workspace.workspaceFolders;
        assert.notStrictEqual(folders, undefined);
        assert.notStrictEqual(folders?.length, 0);
        if (folders !== undefined) {
            /** assert that the promise ececList returns is fulfilled */
            let path = folders[0].uri.path;
            let uid = 'a0820e06961411eabb370242ac130002';
            return chai.expect(hover.execList(path, uid)).to.be.fulfilled;
        }
    });

    test('resolved promise from execList should be a nested array of strings', async () => {
        /** assert that a workspace is open */
        let folders = vscode.workspace.workspaceFolders;
        assert.notStrictEqual(folders, undefined);
        assert.notStrictEqual(folders?.length, 0);
        if (folders !== undefined) {
            /** call execList */
            let path = folders[0].uri.path;
            let uid = 'a0820e06961411eabb370242ac130002';
            let resArray = await hover.execList(path, uid);
            /** assert that execList returns a nested array of strings */
            chai.assert.isArray<string[][]>(resArray);
            resArray.forEach((line) => {
                chai.assert.isArray<string[]>(line);
            });
        }
    });
});