export const treqsElement = {
    "message":
        '<treqs-element id="" type="unit-test">\n\nunit test for feature in sidebar\nadd additional information/details here\n</treqs-element>\n',
    "error": false
};
export const uids = 
{
    "message": "\n20ab4a3ceae411eb8e75acde48001122\n20ab4baeeae411eb8e75acde48001122\n20ab4beaeae411eb8e75acde48001122\n20ab4c1ceae411eb8e75acde48001122\n20ab4c44eae411eb8e75acde48001122\n",
    "error": false
};
export const uidsError = 
{
    "message": "treqs id generation failed, amount should be a integer and cannot be more than 30",
    "error": true
};
export const treqsLink = {
    "message":'<treqs-link type="relatesTo" target="20ab4a3ceae411eb8e75acde48001122" />\n',
    "error": false,
};
export const emptyTreqsElement = {
    "message":
    '<treqs-element id="" type="">\n\n\nadd additional information/details here\n</treqs-element>\n',
    "error": false
};
export const emptyTreqsLink = {
    "message":'<treqs-link type="" target="" />\n',
    "error": false,
};
export const errorTreqsElement = {
    "message":
        "Treqs element creation failed, are you sure that you have Treq's installed",
    "error": true
};
export const errorTreqsLink = {
    "message": "Treqs link creation failed, are you sure that you have Treq's installed",
    "error": true,
};

export const treqsList = {
    "message": '<table>\n<thead>\n<tr>\n<th>UID</th>\n<th>Type</th>\n<th>Issue</th>\n</tr>\n</thead>\n</table>\n',
    "error": false
}
export const errorTreqsList = {
    "message":"Treqs list failed, are you sure that you have Treq's installed",
    "error": true}

