
export const tableView = (mdAsHtml: string) => {
    return `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            table{
                border-collapse: collapse;
                border-spacing: 0;
                border:1.5px solid #181818;
                margin-top: 5px;
            }
            th{
                border:1.5px solid #181818;
            }
            td{
                border:1px solid #181818;
                padding:5px;
            }
        </style>
        <title>Document</title>
    </head>
    <body>
        ${mdAsHtml}
    </body>
    </html>`;
};