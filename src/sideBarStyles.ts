export const sideBarStyle: string = 
`
:root {
  --container-paddding: 20px;
  --input-padding-vertical: 6px;
  --input-padding-horizontal: 4px;
  --input-margin-vertical: 4px;
  --input-margin-horizontal: 0;
}

body {
  padding: 0 var(--container-paddding);
  color: var(--vscode-foreground);
  font-size: var(--vscode-font-size);
  font-weight: var(--vscode-font-weight);
  font-family: var(--vscode-font-family);
}

ol,
ul {
  padding-left: var(--container-paddding);
}

body > *,
form > * {
  margin-block-start: var(--input-margin-vertical);
  margin-block-end: var(--input-margin-vertical);
}

*:focus {
  outline-color: var(--vscode-focusBorder) !important;
}

a {
  color: var(--vscode-textLink-foreground);
}

a:hover,
a:active {
  color: var(--vscode-textLink-activeForeground);
}

code {
  font-size: var(--vscode-editor-font-size);
  font-family: var(--vscode-editor-font-family);
}

h3 {
  margin-top: 8px;
  margin-bottom: 8px;
}

button {
  border: none;
  padding: var(--input-padding-vertical) var(--input-padding-horizontal);
  width: 100%;
  text-align: center;
  outline: 1px solid transparent;
  outline-offset: 2px !important;
  color: var(--vscode-button-foreground);
  background: var(--vscode-button-background);
}

button:hover {
  cursor: pointer;
  background: var(--vscode-button-hoverBackground);
}

button:focus {
  outline-color: var(--vscode-focusBorder);
}

button.secondary {
  color: var(--vscode-button-secondaryForeground);
  background: var(--vscode-button-secondaryBackground);
}

button.secondary:hover {
  background: var(--vscode-button-secondaryHoverBackground);
}

input:not([type="checkbox"]),
textarea {
  display: block;
  width: 100%;
  border: none;
  font-family: var(--vscode-font-family);
  padding: var(--input-padding-vertical) var(--input-padding-horizontal);
  color: var(--vscode-input-foreground);
  outline-color: var(--vscode-input-border);
  background-color: var(--vscode-input-background);
}

input::placeholder,
textarea::placeholder {
  color: var(--vscodse-input-placeholderForeground);
}

.hiddenCheck {
  display: none;
}

#btnControl1:checked + #lable1 > #create {
  height: 470px;
  border: 1px solid #0078d7;
  transition: all 0.1s ease;
}

#btnControl2:checked + #lable2 > #list {
  height: 300px;
  border: 1px solid #0078d7;
  transition: all 0.1s ease;
}
#btnControl3:checked + #lable3 > #process {
  height: 220px;
  border: 1px solid #0078d7;
  transition: all 0.1s ease;
}
#btnControl4:checked + #lable4 > #check {
  height: 200px;
  border: 1px solid #0078d7;
  transition: all 0.1s ease;
}
#btnControl5:checked + #lable5 > #help {
  height: 200px;
  border: 1px solid #0078d7;
  transition: all 0.1s ease;
}

#create_container {
  position: relative;
  top: -24px;
  width: 92%;
  margin: 10px;
}

#list_container {
  position: relative;
  top: -15px;
  width: 92%;
  margin: 10px;
}

#process_container {
  position: relative;
  top: -15px;
  width: 92%;
  margin: 10px;
}

#check_container {
  position: relative;
  top: 50px;
  width: 92%;
  margin: 10px;
}

#help_container {
  position: relative;
  top: 50px;
  width: 92%;
  margin: 10px;
}
.answer_list {
  position: relative;
  top: 50px;
  width: 92%;
  margin: 10px;
}

svg {
  bottom: 15px;
  position: relative;
  width: 20px;
  height: 20px;
}
.item {
  height: 54px;
  margin-left: 10px;
  margin: 0;
  border: 1px solid #3e3e3e;
  border-radius: 2px;
  padding: 10px;
  width: 100%;
  border-bottom-style: hidden;
}

input{
  height: 25px;
}
.downArrow {
  position: relative;
  top: 6px;
  left: 2px;
  width: 15px;
  height: 8px;
}

.arrow {
  transform: rotate(270deg);
}

#row {
  height: 100%;
  width: 100%;
  margin: 0 auto;
}

#help {
  margin: 0;
  border: 1px solid #3e3e3e;
  border-radius: 2px;
  padding: 6px;
  width: 100%;
  display: inline-block;
}

.title {
  top: 5px;
  position: relative;
  left: 23px;
}

#btnControl {
  display: none;
}

.create_button {
  margin-top: 12px;
  margin-bottom: 10px;
}

#btnControl:checked + label > img {
  width: 50%;
  height: 50%;
  z-index: 1;
  -webkit-transition: all 0.25s ease;
  -moz-transition: all 0.25s ease;
  -ms-transition: all 0.25s ease;
  -o-transition: all 0.25s ease;
  transition: all 0.25s ease;
}

html {
  box-sizing: border-box;
  font-size: 13px;
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

body,
h1,
h2,
h3,
h4,
h5,
h6,
p,
ol,
ul {
  margin: 0;
  padding: 0;
  font-weight: normal;
}

img {
  max-width: 100%;
  height: auto;
}

#title {
  margin: auto;
}

.list_input{
  margin-bottom: 12px;
}
select {
  background-color: transparent;
  border: none;
  margin: 0;
  width: 100%;
  font-family: inherit;
  color: #ffffff;
  font-size: inherit;
  cursor: inherit;
}
.select {
  margin-bottom: 12px;
  align-items: center;
  position: relative;
  height: 25px;
  padding: 0.25em 0.5em;
  font-size: 1.1rm;
  cursor: pointer;
  line-height: 1.1;
  background-color: #3C3C3C;
}
`;
