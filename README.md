# TReqs

TReqs is an extension for the TReqs: Tool Support for Managing Requirements in Large-Scale Agile System Development.

TReqs tool is accessible at https://gitlab.com/treqs-on-git/treqs-ng.

## Requirements

You are required to install those dependencies on your device:

* [TReqs core](https://gitlab.com/treqs-on-git/treqs-ng)

Extra dependencies that unlock extra features:

* [Graphviz Preview](https://marketplace.visualstudio.com/items?itemName=EFanZh.graphviz-preview)
* [Graphviz](https://graphviz.org/download/)

## Features

TReqs extension helps you to control the core of TReqs in your Visual Studio Code, allowing you to use it more dynamic and comfortable.


### `TReqs list`

    here comes video preview

### **TReqs check**
<br>

#### TReqs check table

    here comes video preview

#### TReqs check diagram

*__NOTE:__* This feature require you to install extra dependencies.

    here comes video preview

### `TReqs process`

    here comes video preview

### `TreqsBar`

TReqs Bar allows you to create element and generate uuids just in couple clicks without any need to use a terminal.

You can find also other functions in the bar such as:
* list treqs elements.
* check treqs elements.
* process treqs elements.
* extra help information.

## Known Issues

* [Issues](https://gitlab.com/treqs-on-git/treqs-core-vscode/-/issues)

## Release Notes

### **0.0.5** - 30-06-2021

* Added side view.
* Added shortcuts to create an element and create a link.
* Added TReqs check, TReqs list, TReqs process commands.
* Added keybinding shortcut to generate a new ID.

<br>

**Enjoy!**


*__NOTE:__* 

Sidebar icon is made by the Treq's team under Creative Commons Attribution-ShareAlike 4.0 International License 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br /><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"></a>

Extension icon is made by [Fabiana Antonioli, VE](https://thenounproject.com/term/t-rex-dinosaur/3342447/) according to the [Creative Commons License](https://creativecommons.org/licenses/by/3.0/us/legalcode).

