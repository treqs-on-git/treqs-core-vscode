# Change Log


## **0.0.5** - 30-06-2021

### Added
* !treqs-element, !treqs-link snippets.
* TReqs check, TReqs list, TReqs process commands.
* keybinding shortcut to generate a new ID.
* TReqsBar side view.
    * Create treqs element.
    * Generate more than one IDs.
    * Help section.
